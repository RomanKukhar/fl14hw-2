const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routes/authRouter');

const PORT = process.env.PORT || 8080;

const app = express();

app.use(express.json());
app.use('/api', authRouter);



const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://test:test123@cluster0.juqya.mongodb.net/auth-nodeJS?retryWrites=true&w=majority')
        app.listen(PORT, ()=>console.log(`server started on port ${PORT}`)   )
    } catch (e) {
        console.log(e);
    }
}

start();