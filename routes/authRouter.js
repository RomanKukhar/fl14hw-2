const Router = require('express');
const router = new Router();
const controller = require('../authController');
const profile = require('../profileController');
const {check} = require("express-validator");
const authMiddleware = require('../middleware/authMiddleware')


router.post('/auth/register', [
    check("username", "User name can`t be empty").notEmpty(),
    check('password', "Password must be > 4 < 10").isLength({min: 4, max: 10})
], controller.registration);
router.post('/auth/login', controller.login);
router.get('/users', authMiddleware, controller.getUsers);


module.exports = router;